// Make sure your manifest file specify that your program is DPI aware!

#pragma once

#include <windows.h>

void InitDPIAware();

int DPIAware_x(int x);
int DPIAware_y(int y);
