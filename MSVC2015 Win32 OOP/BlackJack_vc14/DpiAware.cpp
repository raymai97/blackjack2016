#include "DpiAware.h"

int dpi_x = 96, dpi_y = 96;

void InitDPIAware() {
    HDC screen_hdc = GetDC(nullptr);
	dpi_x = GetDeviceCaps(screen_hdc, LOGPIXELSX);
	dpi_y = GetDeviceCaps(screen_hdc, LOGPIXELSY);
    ReleaseDC(nullptr, screen_hdc);
}

int DPIAware_x(int x) {
    return x * dpi_x / 96;
}

int DPIAware_y(int y) {
    return y * dpi_y / 96;
}
