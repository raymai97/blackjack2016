#pragma once
#include <assert.h>
#include <vector>
#include <random>
#include "EnumIterator.h"

namespace BlackJack {

	enum class Event {
		None,
		TakeCard,
		Busted,
		DealWin,
		Exact21,
		FiveCard,
		GameReset
	};

	enum class Involver {
		None,
		Player,
		Com,
		Both
	};

	typedef void(*EventHandler)(Event, Involver);

	enum class CardSuit {
		Spade = 1,
		Heart = 2,
		Club = 3,
		Diamond = 4,
		_END_
	};

	enum class CardRank {
		Ace = 1,
		Two = 2,
		Three = 3,
		Four = 4,
		Five = 5,
		Six = 6,
		Seven = 7,
		Eight = 8,
		Nine = 9,
		Ten = 10,
		Jack = 11,
		Queen = 12,
		King = 13,
		_END_
	};

	class Card {
		CardRank rank;
		CardSuit suit;
	public:
		Card(CardRank, CardSuit);
		CardRank GetRank() const;
		CardSuit GetSuit() const;
	};

	typedef std::vector<Card> Cards;

	class Game {
		volatile bool ready = false;
		Cards com_cards, player_cards, unused_cards;
		
		void SetEventOfWinLose();
		void ComTurn();
		int  GetPointOf(Cards const &) const;
		Card TakeRandomCard();	

		EventHandler event_handler = nullptr;
		Event last_event = Event::None;
		Involver last_involver = Involver::None;
		void InvokeLastEvent() const;

	public:
		Game();
		void Deal();
		void TakeCard();
		void Reset();

		Cards const & GetComCards() const;
		Cards const & GetPlayerCards() const;	
		int GetComPoint() const;
		int GetPlayerPoint() const;

		void GetLastEvent(Event&, Involver&) const;
		void SetEventHandler(EventHandler);
	};

	class Exception {
	};

	class OutOfUnusedCardException : Exception {
	};

	class GameNotReadyException : Exception {
	};
	

}
