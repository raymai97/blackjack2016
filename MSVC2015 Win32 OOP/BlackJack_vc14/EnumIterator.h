#pragma once

template<typename T, T first, T last>
class EnumIter : public std::iterator<std::forward_iterator_tag, T> {
	T val;

public:
	inline EnumIter() : val(first) {}
	inline EnumIter(T val) : val(val) {}

	inline auto begin() -> EnumIter {
		return EnumIter(first);
	}

	inline auto end() -> EnumIter {
		return EnumIter(last);
	}

	// *it
	inline auto operator*() -> T& {
		return val;
	}

	// while (it != last)...
	inline auto operator!=(EnumIter it) -> bool {
		return val != it.val;
	}

	// ++it
	inline auto operator++() -> EnumIter& {
		val = static_cast<T>(static_cast<int>(val)+1);
		return *this;
	}

	// it++
	inline auto operator++(int) -> EnumIter {
		auto ret = *this;
		++(*this);
		return ret;
	}


};
