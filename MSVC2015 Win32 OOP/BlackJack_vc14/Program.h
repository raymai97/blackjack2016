#pragma once
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WindowsX.h>
#include <tchar.h>
#include <sstream>
#include "DpiAware.h"
#include "BlackJack.h"

#ifdef _UNICODE
typedef std::wstringstream ss;
#else
typedef std::stringstream ss;
#endif

#define CLASS_NAME	_T("BlackJack Win32 (vc14)")
#define WND_TITLE	_T("BlackJack! (vc14)")
#define WND_STYLE	(WS_OVERLAPPEDWINDOW & ~(WS_MAXIMIZEBOX | WS_THICKFRAME))

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int LogicalFromPoint(int);
void MySetRect(RECT&, int x, int y, int w, int h);

void OnLoad(HWND);
void OnPaint(HWND);
void OnKeyDown(HWND, UINT vk, BOOL fDown, int cRepeat, UINT flags);
void OnLButtonDown(HWND, BOOL fDoubleClick, int x, int y, UINT keyFlags);

void GameEventHandler(BlackJack::Event, BlackJack::Involver);

