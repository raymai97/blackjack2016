#include "Program.h"

HWND hWnd;
HBRUSH hbr_wnd_bg;
HBRUSH hbr_card_bg;
HBRUSH hbr_black;
HFONT hFont;

RECT rc_btn_take_card;
RECT rc_btn_deal;
RECT rc_btn_reset;

int APIENTRY _tWinMain(HINSTANCE hInst, HINSTANCE dummy, LPTSTR dummy2, int nCmdShow) {
	InitDPIAware();
	hbr_wnd_bg = CreateSolidBrush(0x008000);
	hbr_card_bg = CreateSolidBrush(0x00C000);
	hbr_black = (HBRUSH)GetStockObject(BLACK_BRUSH);
	MySetRect(rc_btn_take_card, 120, 266, 100, 40);
	MySetRect(rc_btn_deal,      240, 266, 100, 40);
	MySetRect(rc_btn_reset,     360, 266, 100, 40);

	// Register class, create and show window
	WNDCLASSEX wcex;
	ZeroMemory(&wcex, sizeof(wcex));
	wcex.cbSize = sizeof(wcex);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = hbr_wnd_bg;
	wcex.lpfnWndProc = WndProc;
	wcex.lpszClassName = CLASS_NAME;	
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	if (!RegisterClassEx(&wcex)) { return 1; }
	hWnd = CreateWindowEx(0,
		CLASS_NAME,
		WND_TITLE,
		WND_STYLE,
		CW_USEDEFAULT, 0,
		DPIAware_x(600), DPIAware_y(400),
		nullptr, nullptr,
		nullptr, nullptr);
	if (!hWnd) { return 2; }
	ShowWindow(hWnd, nCmdShow);
	// Create font
	LOGFONT logFont;
	ZeroMemory(&logFont, sizeof(logFont));
	_tcscpy_s(logFont.lfFaceName, _T("Tahoma"));
	logFont.lfHeight = LogicalFromPoint(14);
	logFont.lfOutPrecision = OUT_TT_ONLY_PRECIS;
	hFont = CreateFontIndirect(&logFont);
	// Message loop
	MSG msg;
	while (GetMessage(&msg, nullptr, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM w, LPARAM l) {
	switch (msg) {
	case WM_CREATE:			OnLoad(hWnd); break;
	case WM_DESTROY:		PostQuitMessage(0); break;
	case WM_PAINT:			OnPaint(hWnd); break;
	case WM_KEYDOWN:		HANDLE_WM_KEYDOWN(hWnd, w, l, OnKeyDown); break;
	case WM_LBUTTONDOWN:	HANDLE_WM_LBUTTONDOWN(hWnd, w, l, OnLButtonDown); break;
	default:
		return DefWindowProc(hWnd, msg, w, l);
	}
	return 0;
}

// ..... Win32 essential

int LogicalFromPoint(int point) {
	int dpi_y = DPIAware_y(96);
	return -MulDiv(point, dpi_y, 72);
}

void MySetRect(RECT & rc, int x, int y, int w, int h) {
	rc.left = DPIAware_x(x);
	rc.top = DPIAware_y(y);
	rc.right = rc.left + DPIAware_x(w);
	rc.bottom = rc.top + DPIAware_y(h);
}

// ..... Helper

using namespace BlackJack;

Game game;
bool game_ended = false;

void OnLoad(HWND hWnd) {
	game.SetEventHandler(GameEventHandler);
	game.Reset();
}

void OnPaint(HWND hWnd) {
	RECT client_rect;
	GetClientRect(hWnd, &client_rect);
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	SetBkMode(hdc, TRANSPARENT);
	SelectObject(hdc, hFont);

	auto DrawCardText = [&hdc](RECT rc, Card const & card) {
		ss s;
		switch (card.GetRank()) {
		case CardRank::Ace:		s << _T("A");  break;
		case CardRank::Two:		s << _T("2");  break;
		case CardRank::Three:	s << _T("3");  break;
		case CardRank::Four:	s << _T("4");  break;
		case CardRank::Five:	s << _T("5");  break;
		case CardRank::Six:		s << _T("6");  break;
		case CardRank::Seven:	s << _T("7");  break;
		case CardRank::Eight:	s << _T("8");  break;
		case CardRank::Nine:	s << _T("9");  break;
		case CardRank::Ten:		s << _T("10"); break;
		case CardRank::Jack:	s << _T("J");  break;
		case CardRank::Queen:	s << _T("Q");  break;
		case CardRank::King:	s << _T("K");  break;
		default: s << _T("?");
		}
		rc.top += 3; rc.bottom -= 3;
		rc.left += 6; rc.right -= 6;
		DrawText(hdc, s.str().c_str(), -1, &rc, DT_SINGLELINE | DT_LEFT | DT_TOP);
		DrawText(hdc, s.str().c_str(), -1, &rc, DT_SINGLELINE | DT_RIGHT | DT_BOTTOM);
	};

	auto &player_cards = game.GetPlayerCards();
	auto &com_cards = game.GetComCards();

	RECT rc;
	for (size_t i = 0; i < player_cards.size(); ++i) {
		MySetRect(rc, 100 + (32 * i), 70, 100, 120);
		FillRect(hdc, &rc, hbr_card_bg);
		FrameRect(hdc, &rc, hbr_black);
		DrawCardText(rc, player_cards[i]);
	}
	if (game_ended) {
		for (size_t i = 0; i < com_cards.size(); ++i) {
			MySetRect(rc, 380 - (32 * i), 100, 100, 120);
			FillRect(hdc, &rc, hbr_card_bg);
			FrameRect(hdc, &rc, hbr_black);
			DrawCardText(rc, com_cards[i]);
		}
	}

	UINT fmt = DT_CENTER | DT_SINGLELINE | DT_VCENTER;
	FillRect(hdc, &rc_btn_take_card, hbr_card_bg);
	FrameRect(hdc, &rc_btn_take_card, hbr_black);
	DrawText(hdc, _T("Take &Card"), -1, &rc_btn_take_card, fmt);

	FillRect(hdc, &rc_btn_deal, hbr_card_bg);
	FrameRect(hdc, &rc_btn_deal, hbr_black);
	DrawText(hdc, _T("&Deal"), -1, &rc_btn_deal, fmt);

	FillRect(hdc, &rc_btn_reset, hbr_card_bg);
	FrameRect(hdc, &rc_btn_reset, hbr_black);
	DrawText(hdc, _T("&Reset"), -1, &rc_btn_reset, fmt);

	ss msg;
	msg << _T("Your point = ") << game.GetPlayerPoint();
	if (game_ended) {
		msg << std::endl << _T("COM point = ") << game.GetComPoint();
	}
	GetClientRect(hWnd, &rc);
	rc.top += 3; rc.right -= 6;
	SetTextColor(hdc, 0x00E000);
	DrawText(hdc, msg.str().c_str(), -1, &rc, DT_RIGHT);

	if (game_ended) {
		SetTextColor(hdc, 0xFFFFFF);
		MySetRect(rc, 56, 74, 50, 20);
		DrawText(hdc, _T("You"), -1, &rc, 0);
		MySetRect(rc, 494, 190, 50, 20);
		DrawText(hdc, _T("COM"), -1, &rc, 0);
	}

	EndPaint(hWnd, &ps);
}

void OnKeyDown(HWND hWnd, UINT vk, BOOL fDown, int cRepeat, UINT flags) {
	if (vk == 'C') { game.TakeCard(); }
	if (vk == 'D') { game.Deal(); }
	if (vk == 'R') { game.Reset(); }
}

void OnLButtonDown(HWND hWnd, BOOL fDoubleClick, int x, int y, UINT keyFlags) {
	auto ClickedOn = [x, y](RECT & rc) -> bool {
		return (x >= rc.left && x <= rc.right && y >= rc.top && y <= rc.bottom);
	};
	if (ClickedOn(rc_btn_take_card)) { game.TakeCard(); }
	if (ClickedOn(rc_btn_deal))      { game.Deal(); }
	if (ClickedOn(rc_btn_reset))     { game.Reset(); }
}

void GameEventHandler(Event et, Involver ir) {
	if (et == Event::GameReset) { game_ended = false;  }
	InvalidateRect(hWnd, nullptr, TRUE);

	ss text, title;
	UINT mb_flag = MB_ICONINFORMATION;

	auto SetWinner = [&text, &title, &mb_flag](Involver in) {
		if (in == Involver::Player) {
			text << _T(" Congratulations!");
			title << _T("You win");
		}
		else if (in == Involver::Com) {
			text << _T(" Better luck next time!");
			title << _T("You lose");
			mb_flag = MB_ICONWARNING;
		}
		else {
			auto old = text.str();
			text = ss();
			text << _T("What a rare situation! ") << old;
			title << _T("Draw game");
		}
	};
	
	switch (et)	{
	case Event::Busted:
		if (ir == Involver::Player) {
			text << _T("You have busted!");
			SetWinner(Involver::Com);
		}
		else if (ir == Involver::Com) {
			text << _T("COM has busted!");
			SetWinner(Involver::Player);
		}
		else if (ir == Involver::Both) {
			text << _T("Both players have busted!");
			SetWinner(Involver::Both);
		}
		break;
	case Event::DealWin:
		if (ir == Involver::Player) {
			text << _T("Your point is higher than COM's!");
			SetWinner(Involver::Player);
		}
		else if (ir == Involver::Com) {
			text << _T("COM's point is higher than yours!");
			SetWinner(Involver::Com);
		}
		else if (ir == Involver::Both) {
			text << _T("COM's point is same as yours!");
			SetWinner(Involver::Both);
		}
		break;
	case Event::Exact21:
		if (ir == Involver::Player) {
			text << _T("Your point is exactly 21!");
			SetWinner(Involver::Player);
		}
		else if (ir == Involver::Com) {
			text << _T("COM's point is exactly 21!");
			SetWinner(Involver::Com);
		}
		break;
	case Event::FiveCard:
		if (ir == Involver::Player) {
			text << _T("You've taken 5 cards, yet not over 21.");
			SetWinner(Involver::Player);
		}
		else if (ir == Involver::Com) {
			text << _T("COM has taken 5 cards, yet not over 21.");
			SetWinner(Involver::Com);
		}
		break;
	default:
		return;
	}
	game_ended = true;
	InvalidateRect(hWnd, nullptr, TRUE);
	MessageBox(hWnd, text.str().c_str(), title.str().c_str(), mb_flag);
	game.Reset();
}
