#include "BlackJack.h"

using namespace BlackJack;

// Helper .................

int rnd(int const min, int const max) {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(min, max);
	return dis(gen);
}

// Event Handler ..........

void Game::GetLastEvent(Event & et, Involver & ir) const {
	et = last_event;
	ir = last_involver;
}

void Game::SetEventHandler(EventHandler eh) {
	event_handler = eh;
}

void Game::InvokeLastEvent() const {
	if (event_handler) {
		event_handler(last_event, last_involver);
	}
}

// BlackJack card .........

Card::Card(CardRank rank, CardSuit suit) : rank(rank), suit(suit) {}
CardRank Card::GetRank() const { return rank; }
CardSuit Card::GetSuit() const { return suit; }

// BlackJack game .........

Game::Game() {
}

Cards const & Game::GetComCards() const {
	return com_cards;
}

Cards const & Game::GetPlayerCards() const {
	return player_cards;
}

int Game::GetComPoint() const {
	return GetPointOf(com_cards);
}

int Game::GetPlayerPoint() const {
	return GetPointOf(player_cards);
}

void Game::Deal() {
	if (!ready) { throw GameNotReadyException(); }
	ComTurn();

	SetEventOfWinLose();
	if (last_event == Event::None) {
		int com_point = GetComPoint();
		int player_point = GetPlayerPoint();

		last_event = Event::DealWin;
		last_involver = (
			player_point > com_point ? Involver::Player :
			com_point > player_point ? Involver::Com :
			Involver::Both);
	}
	ready = false;
	InvokeLastEvent();
}

void Game::TakeCard() {
	if (!ready) { throw GameNotReadyException(); }
	player_cards.push_back(TakeRandomCard());
	last_event = Event::TakeCard;
	last_involver = Involver::Player;
	ComTurn();
	SetEventOfWinLose();
	InvokeLastEvent();
}

void Game::Reset() {
	ready = false;
	do {
		unused_cards.clear();
		for (auto rank : EnumIter<CardRank, CardRank::Ace, CardRank::_END_>()) {
			for (auto suit : EnumIter<CardSuit, CardSuit::Spade, CardSuit::_END_>()) {
				unused_cards.push_back(Card(rank, suit));
			}
		}
		com_cards.clear();
		com_cards.push_back(TakeRandomCard());
		com_cards.push_back(TakeRandomCard());
		player_cards.clear();
		player_cards.push_back(TakeRandomCard());
		player_cards.push_back(TakeRandomCard());

		SetEventOfWinLose();
	} while (last_event != Event::None);
	ready = true;
	last_event = Event::GameReset;
	last_involver = Involver::None;
	InvokeLastEvent();
}

void Game::SetEventOfWinLose() {
	int com_point = GetComPoint();
	int player_point = GetPlayerPoint();

	if (player_point > 21 && com_point > 21) {
		last_event = Event::Busted;
		last_involver = Involver::Both;
	}
	else if (player_point > 21) {
		last_event = Event::Busted;
		last_involver =  Involver::Player;
	}
	else if (player_point == 21) {
		last_event = Event::Exact21;
		last_involver = Involver::Player;
	}
	else if (player_cards.size() >= 5) {
		last_event = Event::FiveCard;
		last_involver = Involver::Player;
	}
	else if (com_point > 21) {
		last_event = Event::Busted;
		last_involver = Involver::Com;
	}
	else if (com_point == 21) {
		last_event = Event::Exact21;
		last_involver = Involver::Com;
	}
	else if (com_cards.size() >= 5) {
		last_event = Event::FiveCard;
		last_involver = Involver::Com;
	}
	else {
		last_event = Event::None;
		last_involver = Involver::None;
	}
}

void Game::ComTurn() {
	double rate = (20 - GetComPoint()) / 20.0;
	if (rate + (rnd(1, 5) * 0.1) > 0.5) {
		com_cards.push_back(TakeRandomCard());
		last_event = Event::TakeCard;
		last_involver = Involver::Com;
	}
}

int Game::GetPointOf(Cards const &cards) const {
	int total = 0, ace_count = 0;
	for (auto card : cards) {
		int point = static_cast<int>(card.GetRank());
		if (point > 10) { point = 10; }
		else if (point == 1) { 
			point = 11;
			++(ace_count);
		}
		total += point;
	}
	while (total > 21 && ace_count > 0) {
		--(ace_count);
		total -= 10;
	}
	return total;
}

Card Game::TakeRandomCard() {
	if (unused_cards.size() < 1) { throw OutOfUnusedCardException(); }
	
	auto i = rnd(0, unused_cards.size() - 1);
	auto card = unused_cards[i];
	unused_cards.erase(unused_cards.begin() + i);
	return card;
}



