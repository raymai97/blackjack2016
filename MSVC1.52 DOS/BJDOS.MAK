# Microsoft Visual C++ generated build script - Do not modify

PROJ = BJDOS
DEBUG = 0
PROGTYPE = 6
CALLER = 
ARGS = 
DLLS = 
D_RCDEFINES = -d_DEBUG
R_RCDEFINES = -dNDEBUG
ORIGIN = MSVC
ORIGIN_VER = 1.00
PROJPATH = C:\MY_MSVC\BJDOS\
USEMFC = 0
CC = cl
CPP = cl
CXX = cl
CCREATEPCHFLAG = 
CPPCREATEPCHFLAG = 
CUSEPCHFLAG = 
CPPUSEPCHFLAG = 
FIRSTC =             
FIRSTCPP = PROGRAM.CPP 
RC = rc
CFLAGS_D_DEXE = /nologo /G2 /W3 /Zi /AM /Od /D "_DEBUG" /D "_DOS" /FR /Fd"BJDOS.PDB"
CFLAGS_R_DEXE = /nologo /Gs /W4 /AM /Oa /Ow /Oe /Og /Oi /Ol /Op /Or /Ot /Os /Ox /Oz /D "NDEBUG" /D "_DOS" /FR 
LFLAGS_D_DEXE = /NOLOGO /NOI /STACK:5120 /ONERROR:NOEXE /CO 
LFLAGS_R_DEXE = /NOLOGO /NOI /STACK:2048 /EXEPACK /ONERROR:NOEXE 
LIBS_D_DEXE = oldnames mlibce 
LIBS_R_DEXE = oldnames 
RCFLAGS = /nologo
RESFLAGS = /nologo
RUNFLAGS = 
OBJS_EXT = 
LIBS_EXT = 
!if "$(DEBUG)" == "1"
CFLAGS = $(CFLAGS_D_DEXE)
LFLAGS = $(LFLAGS_D_DEXE)
LIBS = $(LIBS_D_DEXE)
MAPFILE = nul
RCDEFINES = $(D_RCDEFINES)
!else
CFLAGS = $(CFLAGS_R_DEXE)
LFLAGS = $(LFLAGS_R_DEXE)
LIBS = $(LIBS_R_DEXE)
MAPFILE = nul
RCDEFINES = $(R_RCDEFINES)
!endif
!if [if exist MSVC.BND del MSVC.BND]
!endif
SBRS = PROGRAM.SBR \
		DRAWCHAR.SBR \
		BJACK.SBR \
		DRAWGAME.SBR \
		GAMELOOP.SBR \
		GENLIST.SBR


PROGRAM_DEP = c:\my_msvc\bjdos\program.h \
	c:\my_msvc\bjdos\bjack.h \
	c:\my_msvc\bjdos\genlist.h


DRAWCHAR_DEP = c:\my_msvc\bjdos\program.h \
	c:\my_msvc\bjdos\bjack.h \
	c:\my_msvc\bjdos\genlist.h


BJACK_DEP = c:\my_msvc\bjdos\bjack.h \
	c:\my_msvc\bjdos\genlist.h


DRAWGAME_DEP = c:\my_msvc\bjdos\program.h \
	c:\my_msvc\bjdos\bjack.h \
	c:\my_msvc\bjdos\genlist.h


GAMELOOP_DEP = c:\my_msvc\bjdos\program.h \
	c:\my_msvc\bjdos\bjack.h \
	c:\my_msvc\bjdos\genlist.h


GENLIST_DEP = c:\my_msvc\bjdos\genlist.h


all:	$(PROJ).EXE $(PROJ).BSC

PROGRAM.OBJ:	PROGRAM.CPP $(PROGRAM_DEP)
	$(CPP) $(CFLAGS) $(CPPCREATEPCHFLAG) /c PROGRAM.CPP

DRAWCHAR.OBJ:	DRAWCHAR.CPP $(DRAWCHAR_DEP)
	$(CPP) $(CFLAGS) $(CPPUSEPCHFLAG) /c DRAWCHAR.CPP

BJACK.OBJ:	BJACK.CPP $(BJACK_DEP)
	$(CPP) $(CFLAGS) $(CPPUSEPCHFLAG) /c BJACK.CPP

DRAWGAME.OBJ:	DRAWGAME.CPP $(DRAWGAME_DEP)
	$(CPP) $(CFLAGS) $(CPPUSEPCHFLAG) /c DRAWGAME.CPP

GAMELOOP.OBJ:	GAMELOOP.CPP $(GAMELOOP_DEP)
	$(CPP) $(CFLAGS) $(CPPUSEPCHFLAG) /c GAMELOOP.CPP

GENLIST.OBJ:	GENLIST.CPP $(GENLIST_DEP)
	$(CPP) $(CFLAGS) $(CPPUSEPCHFLAG) /c GENLIST.CPP

$(PROJ).EXE::	PROGRAM.OBJ DRAWCHAR.OBJ BJACK.OBJ DRAWGAME.OBJ GAMELOOP.OBJ GENLIST.OBJ $(OBJS_EXT) $(DEFFILE)
	echo >NUL @<<$(PROJ).CRF
PROGRAM.OBJ +
DRAWCHAR.OBJ +
BJACK.OBJ +
DRAWGAME.OBJ +
GAMELOOP.OBJ +
GENLIST.OBJ +
$(OBJS_EXT)
$(PROJ).EXE
$(MAPFILE)
c:\msvc\lib\+
c:\msvc\mfc\lib\+
$(LIBS)
$(DEFFILE);
<<
	link $(LFLAGS) @$(PROJ).CRF

run: $(PROJ).EXE
	$(PROJ) $(RUNFLAGS)


$(PROJ).BSC: $(SBRS)
	bscmake @<<
/o$@ $(SBRS)
<<
